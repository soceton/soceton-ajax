$(document).ready(function(){
    var apologies = "<div class='alert alert-info'>Apologies, something went wrong!</div>";
    function pushStateURL(url){
        targetUrl = url;
        window.history.pushState(null, null, targetUrl);
    }

    function goBack(){
        window.history.back();
    }

    $(document).on("click", "#mainModalDismiss", function() {
        goBack();
    });

    $('#mainModal').on('hidden.bs.modal', function () {
        goBack();
    });

    function json_to_list_html(json_obj){
        var html = "<ul>";
        $.each(json_obj, function( index, value ) {
            html += "<li>"+value+"</li>";
        });
        html += "</ul>";

        return html;
    }

    function submit_disable(form, status){
        $(form).each(function(){
            $(this).find(':submit').prop("disabled", status);
        });
    }

    function showToggle(divAlert){
        $(divAlert).removeClass('d-none');
    }

    function loadingGIF(width=64){
        return "<img class='mx-auto d-block' width='"+width+"' src='https://ajax.soceton.com/loading.gif' alt='loading..' />"
    }

    function ajaxForm(form){
        submit_disable(form, true);
        var alert_div = $(form).find("div#modalAlert");
        $(alert_div).html(loadingGIF());
        showToggle(alert_div);
        
        $.ajax({
            type: $(form).attr("method"),
            url: $(form).attr("action"),
            data: $(form).serialize(),
            success: function(response){
                if(response.success){
                    $(alert_div).removeClass("alert-danger");
                    $(alert_div).addClass("alert-success");
                    $(alert_div).html(json_to_list_html(response.data.messages));

                    if ($(form).attr("soceton-redirect-id"))
                        if (response.data.meta.id)
                            window.location.href = $(form).attr("soceton-redirect-id") + "" + response.data.meta.id;
                        else submit_disable(form, false);
                    else submit_disable(form, false);
                }else{
                    $(alert_div).removeClass("alert-success");
                    $(alert_div).addClass("alert-danger");
        
                    $(alert_div).html(json_to_list_html(response.data));
                    submit_disable(form, false);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(content_div).html(apologies);
            }
        });
    }

    function modalAjax(source){
        var content_div = $(source).attr("data-target") + " .modal-content";
        $(content_div).html(loadingGIF(128));
        var method = $(source).attr("soceton-method");
        var requestData = $(source).attr("soceton-data");
        
        $.ajax({
            type: method,
            url: $(source).attr("soceton-content-url"),
            data: requestData,
            success: function(response){
                $(content_div).html(response);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(content_div).html(apologies);
            }
        });
    }

    function blockAjax(source){
        var method = $(source).attr("soceton-method");
        var requestData = $(source).attr("soceton-data");
        var url = $(source).attr("soceton-content-url");
        
        if (!url) {
            $(source).html(apologies);
            return;
        }
        data = {};
        if (requestData) data = requestData;
        
        $.ajax({
            type: method,
            url: $(source).attr("soceton-content-url"),
            data: data,
            success: function(response){
                $(source).html(response);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(source).html(apologies);
            }
        });
    }

    $("div[soceton-block-ajax=true]").each(function(){
        $(this).removeAttr("soceton-block-ajax");
        $(this).html(loadingGIF());
        blockAjax(this);
    });
    
    $(document).on("click", "[soceton-modal-ajax=true]", function(){
        if($(this).attr("soceton-target-page")) pushStateURL($(this).attr("soceton-target-page"));
        modalAjax(this);
    });

    $(document).on("submit", "form[soceton-form-ajax=true]", function(e){
        e.preventDefault();
        ajaxForm(this);
    });
});
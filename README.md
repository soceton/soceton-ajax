# Soceton AJAX

Soceton AJAX is a very simple framework for AJAX, but it will help you to save tons of time of development. 
    It will also help you to develop a AJAX based website in a organized way and keep the code simple and easy.
    The framework is developed with JQuery and Bootstrap. It was also a open source framework, so that, you can modify and use it for free.
    Share your updated code to development community to help other, that won&#39;t need to write that code.
    
    
[Soceton AJAX Website and Tutorial](https://ajax.soceton.com/)